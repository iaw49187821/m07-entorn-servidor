<style>
    img{
        width:300px;
        height:300px;
    }
</style>
<?php
include_once('_header.php');
include('Class/GalleryClass.php');


$galeria = new Gallery("pictures/fotos.txt");
  
    foreach($galeria->getGallery() as $valor) {
       
       ?>
    <div class="d-flex flex-row">
        <div class="card card-blog">
            <div class="card">          
                <div id="div1" ><img src=<?=$valor->getfileName();?> class="w-100 shadow-1-strong rounded" alt=""></div>           
            </div>
        <div class="card-body text-center">
            <h4 class="card-title">
            <?=$valor->getTitle();?>
            </h4>
        </div>
    </div>
     <?}?>
    </div>
<? 
if (sizeof($galeria->getGallery()) == 0) { ?>
    <div class="alert alert-primary" role="alert">
        The gallery is empty
    </div>
<?php
}

include_once('_footer.php'); ?>