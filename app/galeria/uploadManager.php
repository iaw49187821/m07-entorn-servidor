<?php
/*
* Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
* que almacena todas las fotos.
* Return: Devuelve la ruta final del archivo.
*/
include('Class/UploadClass.php');
define("NOMBRE_IMAGEN", 'photo');
define("TITULO_IMAGEN", 'titulo');
define('ERROR_TITULO', "Porfavor escribe un titulo a la imagen");

//echo NOMBRE_IMAGEN;

// Check if the form was submitted
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES[NOMBRE_IMAGEN])) {
    //check if title is in the form
    if (empty($_POST[TITULO_IMAGEN])) {
        header('Location: index.php?upload=error&msg=' . urlencode(ERROR_TITULO));
        return;
    }
    
    $upload = new Upload(NOMBRE_IMAGEN);
    //If File is uploaded correcty, we added to our file

    if ($upload->getFile() != null)
        $upload->addPictureToFile();
    // Any error redirect with error message
    if ($upload->getError() != null)
        header('Location: index.php?upload=error&msg=' . urlencode($upload->getError()));
    else header("Location: index.php?upload=success");
}