<?php include_once('_header.php');?>

    <div class="p-3 mb-2 bg-dark text-white"><h1>Image's Gallery</h1></div>
        <div class="card-body">
            <div class="bd-example">
                <a type="button" class="btn btn-primary" href="addPicture.php">Add picture</a>
                <a type="button" class="btn btn-success" href="gallery.php">View Gallery</a>
            </div>
        </div>
    </div>
<?php include_once('_footer.php');?>