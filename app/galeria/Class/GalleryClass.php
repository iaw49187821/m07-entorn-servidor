<?php
include('PictureClass.php');

class Gallery
{
    private $_gallery = [];
    private $_fileName;

  /*Constructor: Recibe la ruta del archivo fotos.txt*/
    function __construct($fileName){
      $this->_fileName=$fileName;
      $this->loadGallery();
    }

    /*
  *Recorre el archivo fotos.txt y para cada linea, crea un
  *elemento Picture que lo añade al atributo $_gallery[]
  */
    function loadGallery(){
      $fichero = fopen($this->_fileName,"r");
      

      while(!feof($fichero)){
        
      
        $linea = fgets($fichero);
        $porciones = explode("###", $linea);
        
        $rutaImagen=$_SERVER['DOCUMENT_ROOT'] . '/'. $porciones[0];

        if ($porciones[0] && $this->is_image($rutaImagen)) {
          $this->_gallery[] = new Picture($porciones[1],$porciones[0]);
      }
      }
      
      fclose($fichero);
      
    }
    
    public function is_image($path)
    {
        $info = getimagesize($path);
        $image_type = $info[2];

        if (in_array($image_type, array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP))) {
            return true;
        }
        return false;
    }

    /*
  *Getters.
  */
    public function getGallery(){
      return $this->_gallery;
    }
}
?>