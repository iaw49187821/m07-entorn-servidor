<?php

class Picture
{
  
    private $_titulo;
    private $_name;

    /*Constructor*/
    function __construct($titulo, $name){
        $this->_name=$name;
        $this->_titulo=$titulo;
     
    }
        

    /*
  *Getters. Lo que quiere decir que los atributos de
  *title y filename son private
  */
    public function setFileName($name){
    $this->_name=$name;
    }
    
    public function setTitle($titulo){
        $this->_titulo=$titulo;
    }

    public function getTitle(){
        return $this->_titulo;

    }
    public function getfileName(){
        return $this->_name;
    }
}
?>