<?php
/*
* Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
* que almacena todas las fotos.
* Return: Devuelve la ruta final del archivo.
*/
define("RUTA", "/galeria/pictures/");
include('Class/PictureClass.php');

class UploadError extends Exception
{
}

class Upload{
    private $filename = null;
    private $file = null;
    private $error = null;
    function __construct($filename)
    {
        $this->$filename = $filename;
        $this->uploadPicture();
    }

    function uploadPicture()
    {
        try {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                
                if ($_FILES[$this->fileName]["error"] != 0)
                    throw new UploadError("Error: " . $_FILES["picture"]["error"]);

                $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
                $filename = $_FILES["photo"]["name"];
                $filetype = $_FILES["photo"]["type"];
                $filesize = $_FILES["photo"]["size"];

                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (!array_key_exists($ext, $allowed)) {
                    throw new UploadError("Error: Seleccione un formato valido.");
                }

                $maxsize = 5 * 1024 * 1024;
                if ($filesize > $maxsize) {
                    throw new UploadError("Error: El fichero supera el tamaño maximo.");
                }


                // Check whether file exists before uploading it
                if (file_exists(RUTA . $filename)) {
                    throw new UploadError($filename . " ya existe.");
                    
                }
                    
                if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/' . RUTA))
                   mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . RUTA);
      


                if (!is_writeable($_SERVER['DOCUMENT_ROOT'] . '/' . RUTA)) 
                    throw new UploadError("Error: No tienes permisos en la carpeta.");
                

                if (in_array($filetype, $allowed)) {
                    move_uploaded_file($_FILES["photo"]["tmp_name"],  $_SERVER['DOCUMENT_ROOT'] . '/' . RUTA . $filename);
                    $this->file =RUTA . $filename;
                     
                }
            }
        
        } catch (UploadError $e) {
            $this->error = $e->getMessage();
        } catch (Exception $e) {
            $this->error = $e->getMessage();
        }
        
    }

    /*
    * Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
    * fotografía recien subida
    * Entradas:
    *       $file_uploaded: La ruta del archivo
    *       $title_uploaded: El titulo del archivo
    * Return: null
    */
    function addPictureToFile()
    {
        $titulo = $_POST["titulo"];
        $fichero = fopen($_SERVER['DOCUMENT_ROOT'] . '/' . RUTA ."fotos.txt", "a+");
        fputs($fichero, $this->file . "###" . $titulo . "\n");
        fclose($fichero);
    }

    function getError()
    {
        return $this->error;
    }
    function getFile()
    {
        return $this->file;
    }
}
/*
* Clase personalizada extendida de Exception que utilizaremos para lanzar errores
* en la subida de archivos. Por ejemplo:
* throw new UploadError("Error: Please select a valid file format.");
*/

