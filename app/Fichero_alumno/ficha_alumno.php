<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<?php
?>

<div class="p-3 mb-2 bg-dark text-white"><h1>Upload person</h1></div>

<form method="post" action="ficha_alumno_view.php" enctype="multipart/form-data">
				<table class="table table-striped">
                <tbody>
						<tr>
							<th>Name:</th>
                            <th><input type="text" name="nombre" /></th>							
						</tr>			
						<tr>
                        <th>Surname:</th>
                            <th><input type="text" name="apellido" /></th>							
							
						</tr>
						<tr>
                        <th>Address:</th>
                            <th><input type="text" name="direccion" /></th>							
						
						</tr>
						<tr>
                        <th>Comments:</th>
                            <th><input type="text" name="comentario" /></th>							
						 
							
						</tr>
						<tr>
                        <th>Picture:</th>
                            <th><input type="file" name="photo" id="fileSelect"/></th>							
						
						</tr>
						<tr>
							<th><input type="submit" value="Upload"/></th>
						</tr>
        
                </table>
</form>