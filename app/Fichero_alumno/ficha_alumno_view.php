<?php
include('PersonaClass.php');
include('UploadClass.php');

$upload = new Upload('photo');
$alumno = new Persona();

$alumno->setName($_POST['nombre']);
$alumno->setSurname($_POST['apellido']);
$alumno->setAddress($_POST['direccion']);
$alumno->setComments($_POST['comentarios']);
$alumno->setPicture($upload->getPath());
?>

<!DOCTYPE html>
<body>
<img class="card-img-top" src="<?= $alumno->getPicture() ?>" >
    <div class="card"> 
        Nombre:
<?= $alumno->getName();?>
    </div>
</body>
</html>