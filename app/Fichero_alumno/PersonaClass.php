<?php
class Persona
{
private $_name;
private $_surname;
private $_picture;
private $_address;
private $_comments;

  /*
  * Setters. Para añadir y modificar valores
  */
    public function setName($name){
      $this->_name=$name;
    }
    public function setSurname($surname){
      $this->_surname=$surname;
    }
    public function setAddress($address){
      $this->_address=$address;
    }
  
    public function setPicture($picture){
      $this->_picture=$picture;
    }
    public function setComments($comments){
      $this->_comments=$comments;
    }
    public function setComments($comments){
      $this->_comments=$comments;
    }

    /*
  * Getters. Lo que quiere decir que los atributos de la clase son private
  */
    public function getName(){
      return $this->_name;
    }
    public function getSurname(){
      return $this->_surname;
    }
    public function getPicture(){
      return $this->_picture;
    }
    public function getAddress(){
      return $this->_address;
    }
    public function getComments(){
      return $this->_comments;
    }
}
?> 